import pandas as pd
import random
import math
from datetime import datetime, timedelta
from random import randint
import time
import datetime as dt
import pymongo
from pymongo import MongoClient

connection = MongoClient()
db = connection.get_database('sports')
collection = db.get_collection('injury')

df = pd.read_csv("sport.csv",encoding='utf-8')
df_stream = pd.read_csv("injury_stream.csv",encoding='utf-8')
df_stream.drop(df_stream.index, inplace=True)
df_stream.dropna(axis=0, inplace = True)
df_stream.dropna(axis=1, inplace = True)
df_stream.to_csv('injury_stream.csv', index=False)


def stream_generator(player):
    time = dt.datetime.now()
    name = player
    playerload = 900 + random.uniform(-300, 200)
    if playerload < 1000:
        threashold = 0
    else:
        threashold = int((playerload - 800) / 8)
    speed = playerload / 2 + random.uniform(-50, 50)
    heartrate = int(playerload / 6)
    duration = 1

    row = {'Time': time, 'Name': name, 'Playerload': playerload, 'Threashold': threashold, 'Speed': speed,
           'Heartrate': heartrate, 'Duration': duration}
    return row


def insert_data():
    # records = pd.DataFrame(columns = df.columns)
    collection.delete_many({"Time": {"$lt": (dt.datetime.now() - timedelta(hours=1))}})
    for i in range(200):
        name = 'Sacar Anim'
        collection.insert_one(stream_generator(name))

    for i in range(1000):
        # for name in df['player.name'].unique():
        name = 'Sacar Anim'
        # if i % 100 ==0:
        # collection.delete_many({"Time": {"$lt": (dt.datetime.now() - timedelta(hours = 24))}})
        # if i % 100 == 0 and i >200:
        # ndoc = collection.find({}, ('_id',), limit=100)
        # selector = {'_id': {'$in': [doc['_id'] for doc in ndoc]}}
        # collection.delete_many(selector)
        collection.insert_one(stream_generator(name))
        time.sleep(1)
