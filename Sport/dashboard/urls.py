from django.urls import path, re_path
from . import views
from .data_generator import insert_data
from . import dash_app_base_generic   # this loads the Dash app
from . import dash_layouts
from .dash_server import app, server


app_name = 'dashboard'

urlpatterns = [
                re_path('^_dash-', views.dash_ajax),
                path('index/', views.index, name='index'),
                path('<author_id>/team/', views.dashboard1, name='team'),
                path('<author_id>/player/', views.dashboard2, name='player'),
                path('livingstream/', views.livingstream, name='livingstream'),
              ]
